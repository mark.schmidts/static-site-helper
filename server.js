function isNumber(obj) { return !isNaN(parseFloat(obj)) }

var htdocs_folder = 'vueJsOverview',
port = 8080;

process.argv.forEach(function (val, index, array) {
  if(index>1 && val != "" && val != null && val != undefined){
        if(isNumber(val)==true)
            port = parseInt(val);
        else
            htdocs_folder=val;
  }
});

var url='http://localhost:'+port+'/';

console.log("serving target folder \"" + htdocs_folder+"\"");
console.log("at port "+port);
console.log("url: "+url);

var connect = require('connect');
var serveStatic = require('serve-static');
connect().use(serveStatic(htdocs_folder)).listen(port);

var open = require('open');
open(url);
