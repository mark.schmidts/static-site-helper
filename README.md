# Static Site Helper

*  Are you developing a static website in a local environment?
*  Do you want to try out that website under usual static hosting circumstances?

Than this is your repository.

**Serve your local project with a few some easy commands.**


## Install

In the main directory of this project, run

```shell
npm install
```

## Usage
### HTTP Server

```shell
node server.js dist 2020
```

Where *dist* is your Folder with static files and *2020* is the port you wanna open your server at.

### HTTPS Server

```shell
node serverHttps.js dist 2020
```
Same same, but experimental. Please let me know if it works for you or not.
